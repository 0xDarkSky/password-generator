import random, sys, argparse, secrets, string, re
import pyperclip
 
class PasswordGenerator:
    def __init__(self, lowercase, uppercase, numbers, special, total_length, check, clipboard):
        self.lowercase = lowercase
        self.uppercase = uppercase
        self.numbers = numbers
        self.special = special
        self.total_length = total_length
        self.check = check
        self.clipboard = clipboard
    
    def generate_password(self):
        password = []

        if self.check:
            if(len(self.check)>=8):
                if(bool(re.match('((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,30})', self.check))==True):
                    print("The password is strong")
                elif(bool(re.match('((\d*)([a-z]*)([A-Z]*)([!@#$%^&*]*).{8,30})', self.check))==True):
                    print("The password is weak")
            exit()
                
        if self.total_length:
            password.append("".join(
            [secrets.choice(string.digits + string.ascii_letters + string.punctuation) \
                for _ in range(self.total_length)]))
            password = ''.join(password)
 
        else:
            for i in range(self.lowercase):
                password.append(secrets.choice(string.ascii_lowercase))
 
            for i in range(self.uppercase):
                password.append(secrets.choice(string.ascii_uppercase))
 
            for i in range(self.numbers):
                password.append(secrets.choice(string.digits))
 
            for i in range(self.special):
                password.append(secrets.choice(string.punctuation))
 
            random.shuffle(password)
            password = ''.join(password)
 
        if self.clipboard:
            pyperclip.copy(password)
 
        return password
 
class ArgParser:
    def __init__(self):
        self.parser = argparse.ArgumentParser(formatter_class = argparse.RawDescriptionHelpFormatter)
        self.parser.add_argument("-l", "--lowercase", help = "Lowercase letters", type=int)
        self.parser.add_argument("-u", "--uppercase", help = "Uppercase letters", type=int)
        self.parser.add_argument("-n", "--numbers", help = "Numbers", type=int)
        self.parser.add_argument("-s", "--special", help = "Special characters", type=int)
        self.parser.add_argument("-c", "--clipboard", action="store_true", help="Copy password to clipboard")
 
        self.parser.add_argument("-t", "--total-length", type=int, help="The total password length")
        self.parser.add_argument("-x", "--check", type=str, help="Check password strength")

        if len(sys.argv) == 1:
           self.parser.print_help(sys.stderr)
           exit()
    
    def parse_arguments(self):
        args = self.parser.parse_args()
        return args
 
def main():
    arg_parser = ArgParser()
    args = arg_parser.parse_arguments()
 
    password_generator = PasswordGenerator(args.lowercase, args.uppercase, args.numbers, args.special, args.total_length, args.check, args.clipboard)
    password = password_generator.generate_password()
    print(password)
 
if __name__ == '__main__':
    main()